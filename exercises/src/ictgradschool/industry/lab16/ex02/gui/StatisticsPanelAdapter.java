package ictgradschool.industry.lab16.ex02.gui;

import ictgradschool.industry.lab16.ex02.model.Course;
import ictgradschool.industry.lab16.ex02.model.CourseListener;

public class StatisticsPanelAdapter implements CourseListener{

	private StatisticsPanel statistics;

	public StatisticsPanelAdapter(StatisticsPanel statistics) {
		this.statistics = statistics;
	}

	public void courseHasChanged(Course course){
		statistics.repaint();
	}
}
