package ictgradschool.industry.lab16.ex02.gui;

import ictgradschool.industry.lab16.ex02.model.Course;
import ictgradschool.industry.lab16.ex02.model.CourseListener;


public class DistributionPanelAdapter implements CourseListener {

	private DistributionPanel distribution;

	public DistributionPanelAdapter(DistributionPanel distribution) {
		this.distribution = distribution;
	}

	public void courseHasChanged(Course course){
		distribution.repaint();
	}
}
