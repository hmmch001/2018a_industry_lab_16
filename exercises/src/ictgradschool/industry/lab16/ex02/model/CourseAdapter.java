package ictgradschool.industry.lab16.ex02.model;


import javax.swing.table.AbstractTableModel;

public class CourseAdapter extends AbstractTableModel implements CourseListener {

	private Course course;

	public CourseAdapter(Course course) {
		this.course = course;
		course.addCourseListener(this);
	}

	public void courseHasChanged(Course course){
		fireTableDataChanged();
	}

	public int getRowCount(){
		return course.size();

	}

	public int getColumnCount(){
		return 7;
	}

	public String getColumnName(int column) {
		if(column == 0 ){
			return "Student ID";
		}else if( column == 1){
			return "Surname";
		}else if( column == 2){
			return "Forename";
		}else if( column == 3){
			return "Exam";
		}
		else if( column == 4){
			return "Test";
		}
		else if( column == 5){
			return "Assignment";
		}else{
			return "Overall";
		}
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		StudentResult result = course.getResultAt(rowIndex);
//		String stringResult = result.toString();
//		String [] arrayResult = stringResult.split(",");
//		return arrayResult[columnIndex];
		if(columnIndex == 0 ){
			return result._studentID;
		}else if( columnIndex == 1){
			return result._studentSurname;
		}
		else if( columnIndex == 2){
			return result._studentForename;
		}
		else if( columnIndex == 3){
			return result.getAssessmentElement(StudentResult.AssessmentElement.Exam);
		}
		else if( columnIndex == 4){
			return result.getAssessmentElement(StudentResult.AssessmentElement.Test);
		}
		else if( columnIndex == 5){
			return result.getAssessmentElement(StudentResult.AssessmentElement.Assignment);
		}else{
			return result.getAssessmentElement(StudentResult.AssessmentElement.Overall);
		}

	}

}